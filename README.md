# mat2CIM Testing

This is a testing repository for mat2CIM. It validates CIM xml files generated with mat2CIM. The tests compare the matpower results with the corresponding results obtained with the powerflow solver of DPsim.